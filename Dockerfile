FROM composer:2.5.8 as composer_build
COPY ./composer.json /app/
RUN composer install --no-dev --no-autoloader --no-scripts
COPY . /app
RUN composer update
RUN composer install --no-dev --optimize-autoloader

FROM php:8.2-fpm
COPY .env .
RUN docker-php-ext-install pdo_mysql
COPY --chown=www-data --from=composer_build /app/ /var/www/html/
EXPOSE 9000

